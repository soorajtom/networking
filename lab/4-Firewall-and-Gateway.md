# Firewall and Gateway.

## Session in lab

1. Getting familiar with Iptables

2. Setting machine to be a Gateway.


## Assignment

Use iptables to set up the following setup your laptop as a gateway
and do the following task.

Your machine (the gateway) is NATed in such a way that connection to
port 8000 should appear as connecting to port 8000 of machine X (whose
IP address will be given).

You will need two machines to run this. An alternative is to run X as
a virtual machine with a private ip address 192.168.*.* series. Also X
should use your laptop as a gateway.
