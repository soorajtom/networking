#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: Sooraj
"""

import numpy as np
import random
np.set_printoptions(threshold='nan')

def findParam(r):
    n = (2**r) - 1
    k = r
    return n, k

def genMat(r):
    n, k = findParam(r)
    gen = []
    for i in range(k):
        row = [0 for j in range(k)]
        row[i] = 1
        gen.append(row)
    for i in range(1, n+1):
        row = [(i/(2**(k - j - 1)))%2 for j in range(k)]
        if(np.sum(row) == 1):
            continue
        gen.append(row)
    return np.matrix(gen)

def dEncode(m, r):
    genM = genMat(r)
#    print "Generator matrix:"
#    print genM
    codedM = genM.dot(m)%2
    return np.ravel(codedM)

def generateCode(r):
    n, k = findParam(r)
    codes = []
    for i in range (0, 2**k):
        m = [(i/(2**(k - j - 1)))%2 for j in range(k)]
        codes.append(dEncode(m, r))
#    print codes
    return codes

def genPCMat(r):
    n, k = findParam(r)
    genM = genMat(r)
    PCM = np.vstack( (genM[k:n].T , np.identity(n-k)) )
    return PCM

def dDecode(c, r):
    codes = generateCode(r)
    n, k = findParam(r)
    PCM = genPCMat(r)
    err = (PCM.T).dot(c)%2
    if(np.sum(err) != 0):
        cfound = 0
        err = [1 for i in range(len(c))]
        for i in range(len(codes)):
            nerr= np.add(codes[i], c)%2
            if(np.sum(err) > np.sum(nerr)):
                err = nerr
                cfound = i
        print "This code contains error(s)"
        m = [(cfound/(2**(k - j - 1)))%2 for j in range(k)]
    else:
        m = c[0:k]
    return m
    
if __name__=="__main__":
#    message = [1, 0, 1, 1]
#   [1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1] for 1011
    r = int(raw_input("Enter a value for r: "))
    n, k = findParam(r)
    
    message = [(random.randrange(0, 2, 1)) for i in range(k)]
    print "r:", r
    print "Message: \n", message
    
    a = dEncode(message, r)
    print "Encoded: \n", a
    
    print "\nFlipping no bits:"    
    m = dDecode(a , r)
    print "Decoded: \n", m
    
    for i in range(k - 1):
        print "\nFlipping",  str(i + 1), "bits:"
        flipper = random.randrange(0, k, 1)
        a[flipper] = (a[flipper] + 1) % 2
        m = dDecode(a , r)
        print "Decoded: \n", m
    
    

    