#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: Sooraj
"""

import numpy as np
np.set_printoptions(threshold='nan')

def findParam(r):
    n = (2**r) - 1
    k = (2**r) - r - 1
    return n, k

def generateMat(r):
    gen = []
    n, k = findParam(r)
    for i in range(k):
        row = [0 for j in range(k)]
        row[i] = 1
        gen.append(row)
    for i in range(n - k):
        row = [((i*3)/(2**(j)) + 1)%2 for j in range(k)]
        gen.append(row)
    return np.matrix(gen)

def generatePCheck(r):
    n, k = findParam(r)
    genM = generateMat(r)
    PCM = np.vstack( (genM[k:n].T , np.identity(n-k)) )
#    print PCM
    return PCM
    
def encode(m, r):    #can get r from sizeof(m)
    genM = generateMat(r)
    print genM
    codedM = genM.dot(m)%2
    return np.ravel(codedM)

def decode(c, r):    #can get r from sizeof(c)
    n, k = findParam(r)
#    c = np.reshape(c, (n,1))
    PCheckMat = generatePCheck(r)
    print PCheckMat
    err = (PCheckMat.T).dot(c)%2
    if(np.any(err)):
        print "The code contains error(s)"
        for i in range (n):
            if np.array_equal(PCheckMat[i] , err):
                c[i] = (c[i] + 1)%2
                break
    return c[0:k]

def hamming():
    message = [1, 1, 1, 1]
    r = 3
    print "Message: \n", message
    
    a = encode(message, r)
    print "Encoded: \n", a
    
    print "Decoded: "
    b = decode([1, 1, 1, 1, 0, 1, 1], r)
    print b
    
#hamming()