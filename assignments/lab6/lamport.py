#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 16:04:31 2018
@author: Sooraj
"""

import os
import binascii
from hashlib import sha512

def keygen():
	private_key = []
	public_key = []
	pri_file = open("lamport.key", "w")
	pub_file = open("lamport.pub", "w")
	for i in range(1024):
		private_key.append(binascii.hexlify(os.urandom(64)).decode())
		public_key.append(sha512(private_key[i]).hexdigest())
		pri_file.write(private_key[i])
		pub_file.write(public_key[i])
	pri_file.close()
	pub_file.close()
	return private_key, public_key

def retrieve_keys():
    private_key = []
    public_key = []
    pri_file = open("lamport.key", "r")
    pub_file = open("lamport.pub", "r")
    for i in range(1024):
        private_key.append(pri_file.readline(128))
        public_key.append(pub_file.readline(128))
    return private_key, public_key

def signmess(mess, sig_file_name = 'sig_file', mess_file_name = 'message.txt'):
    signature = []
    private_key, _ = retrieve_keys()
    sig_file = open(sig_file_name, "w")
    mess_file = open(mess_file_name, "w")
    mess_file.write(mess)
    mess_hash = sha512(mess).hexdigest()
    bin_mess_hash = bin(int(mess_hash, 16))[2:].zfill(512)
    for i in range(512):
        if(bin_mess_hash[i] == '0'):
            signature.append(private_key[2*i])
        else:
            signature.append(private_key[2*i + 1])
        sig_file.write(signature[i])
    sig_file.close()
    return mess, signature

def verify(mess_file_name = "message.txt", sig_file_name = 'sig_file'):
    _, public_key = retrieve_keys()
    signature = []
    sig_file = open(sig_file_name, "r")
    mess_file = open(mess_file_name, "r")
    mess = mess_file.read()
    for i in range(1024):
        signature.append(sig_file.readline(128))
    mess_hash = sha512(mess).hexdigest()
    bin_mess_hash = bin(int(mess_hash, 16))[2:].zfill(512)
    verified = 1
    for i in range(512):
        if(bin_mess_hash[i] == '0'):
            if(public_key[i*2] != sha512(signature[i]).hexdigest()):
                verified = 0
                break
        else:
            if(public_key[i*2 + 1] != sha512(signature[i]).hexdigest()):
                verified = 0
                break
    if(verified):
        print "Message is verified"
    else:
        print "message cannot be verified"

#a, b = signmess("Hello")
# verify("Hello")

#print signmess("asd")
#a,b = keygen()

	